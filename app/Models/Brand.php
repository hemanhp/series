<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $fillable = ['title'];
    protected $guarded = ['id'];

    public function series()
    {
        return $this->hasMany(Seri::class);
    }
    
}
